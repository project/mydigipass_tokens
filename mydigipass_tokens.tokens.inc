<?php

/**
 * @file
 * Builds placeholder replacement tokens for MYDIGIPASS.COM data.
 */

/**
 * Implements hook_token_info().
 */
function mydigipass_tokens_token_info() {
  $tokens = array();

  $types['mydigipass'] = array(
    'name' => t('MYDIGIPASS.COM user'),
    'description' => t('MYDIGIPASS.COM tokens related to individual user accounts.'),
    'needs-data' => 'user',
  );

  $types['mydigipass-current'] = array(
    'name' => t('MYDIGIPASS.COM current user'),
    'description' => t('MYDIGIPASS.COM tokens related to current user account.'),
    'type' => 'mydigipass',
  );

  // Extract all available fields which are currently selected.
  $query = db_select('mydigipass_profile_fields', 'mpf')
    ->fields('mpf', array('name', 'title', 'selected'))
    ->orderBy('weight', 'ASC');
  $result = $query->execute();
  foreach ($result as $row) {
    if ($row->selected) {
      // Title can be empty. Default to the name.
      if (empty($row->title)) {
        $row->title = $row->name;
      }

      $tokens[check_plain($row->name)] = array(
        'name' => t('@title', array('@title' => $row->title)),
        'description' => t('The @title value from MYDIGIPASS.COM', array('@title' => $row->title)),
      );
    }
  }

  return array(
    'types' => $types,
    'tokens' => array('mydigipass' => $tokens),
  );
}

/**
 * Implements hook_tokens().
 */
function mydigipass_tokens_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if ($type == 'mydigipass' || $type == 'mydigipass-current') {
    if ($type == 'mydigipass-current') {
      $account = user_load($GLOBALS['user']->uid);
    }
    else {
      $account = $data['user'];
    }
    $sanitize = !empty($options['sanitize']);

    // Extract all available fields which are currently selected.
    $query = db_select('mydigipass_profile_fields', 'mpf')
      ->fields('mpf', array('name', 'selected'))
      ->orderBy('weight', 'ASC');
    $possible_tokens = array();
    $result = $query->execute();
    foreach ($result as $row) {
      if ($row->selected) {
        $possible_tokens[] = $row->name;
      }
    }

    foreach ($tokens as $name => $original) {
      if (in_array($name, $possible_tokens)) {
        $attr_value = db_query('
        SELECT attribute_value FROM {mydigipass_user_data} mud
        JOIN mydigipass_user_link mul
        ON mul.mdp_uuid = mud.mdp_uuid
        WHERE mul.drupal_uid = :uid
        AND mud.attribute_key = :attr_key', array(
          ':uid' => $account->uid,
          ':attr_key' => $name,
        ))->fetchField();

        $replacements[$original] = $sanitize ? check_plain($attr_value): $attr_value;
      }
    }
  }

  return $replacements;
}
