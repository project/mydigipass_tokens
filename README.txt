MYDIGIPASS.COM Tokens
---------------------

Provides tokens for fields acquired from MYDIGIPASS.COM authentication.

Requirements
------------

https://www.drupal.org/project/mydigipass
https://www.drupal.org/project/token

Usage
-----

Enable like any other module.
MYDIGIPASS.COM tokens will be available in all places where tokens are enabled.
For security and usability reasons, only the tokens enabled on admin/config/services/mydigipass/user_data_fields can be used.

Credit
------

Written by rv0 / W. Vanheste for Coworks.be
This module is in no way affiliated with or endorsed by MYDIGIPASS.COM or VASCO Data Security.